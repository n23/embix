;;;
;;; embix - collection of packages and functions for Guix
;;;
;;; Copyright (C) 2023 by Artur Wroblewski <wrobell@riseup.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;; Installation
;;
;; 1. Install Embix as a Guix channel and Git submodule in your repository
;; 2. Generate Guix signing key: sudo guix archive --generate-key
;; 3. Create SSH key: ssh-keygen -f embix/images/vmer -N ''
;; 4. Run:
;;
;;    guix system image --image-type=qcow2 --image-size=4G --save-provenance barevm.scm
;;
;; 5. Guix package manager builds qcow2 image and shows its path.
;; 6. Upload the image to a virtual server solution.

(use-modules (embix))
(use-modules (guix gexp))

(embix-os "barevm" (local-file "vmer.pub"))

;; vim: sw=2:et:ai
