;;;
;;; embix - collection of packages and functions for Guix
;;;
;;; Copyright (C) 2023 by Artur Wroblewski <wrobell@riseup.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;; Installation
;;
;; 1. Deploy barevm machine, see barevm.scm for details.
;; 2. Create "nasvm-smb.conf" file with Samba configuration.
;; 3. Run:
;;
;;    guix deploy nasvm.scm

(use-modules (embix))
(use-modules (gnu services samba))

(define %machine-services
  (cons (service samba-service-type (samba-configuration
          (enable-smbd? #t)
          (config-file (local-file "nasvm-smb.conf"))))
        %embix-services))

(define %system (embix-os "nasvm" (local-file "vmer.pub")
                 #:services %machine-services))

(list (machine
        (operating-system %system)
        (environment managed-host-environment-type)
        (configuration (machine-ssh-configuration
                         (host-name "nasvm")
                         (system "aarch64-linux")
                         (user "vmer")
                         (identity "./vmer")))))

;; vim: sw=2:et:ai
