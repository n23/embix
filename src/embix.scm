;;;
;;; embix - collection of packages and functions for Guix
;;;
;;; Copyright (C) 2023 by Artur Wroblewski <wrobell@riseup.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

(define-module (embix)
  #:use-module (gnu)
  #:use-module (guix)
  #:use-module (gnu packages ssh)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services virtualization)
  #:export (%embix-bootloader
            %embix-fs
            %embix-kbd
            %embix-services
            %embix-sudoers
            %embix-tz
            %embix-users
            embix-os))

(define %embix-bootloader
  (bootloader-configuration
    (bootloader grub-efi-bootloader)
    (targets '("/boot/efi"))
    (terminal-outputs '(console))))

(define %embix-fs
  (append (list (file-system
                  (mount-point "/")
                  (device "/dev/vda2")
                  (type "ext4"))
                (file-system
                  (mount-point "/boot/efi")
                  (device "/dev/vda1")
                  (type "vfat")))
          %base-file-systems))

(define %embix-kbd (keyboard-layout "us" "altgr-intl"))

(define %embix-services
  (append (list (service openssh-service-type
                  (openssh-configuration
                    (openssh openssh-sans-x)
                    (x11-forwarding? #f)
                    (permit-root-login #f)
                    (password-authentication? #f)))
                (service dhcp-client-service-type)
                (service ntp-service-type)
                (service qemu-guest-agent-service-type))
          (modify-services %base-services
            (guix-service-type config =>
              (guix-configuration
                (inherit config)
                (authorized-keys
                  (append (list (local-file "/etc/guix/signing-key.pub"))
                                %default-authorized-guix-keys)))))))

(define %embix-sudoers
  (plain-file "sudoers" "\
root ALL=(ALL) ALL
vmer ALL=(ALL) NOPASSWD: ALL
"))

(define %embix-tz "Etc/UTC")

(define %embix-users
  (cons (user-account
          (name "vmer")
          (group "users")
          (comment "VM provisioning user")
          (supplementary-groups '("wheel")))
        %base-user-accounts))

(define* (embix-os name
                   ssh-pub-key
                   #:optional
                   #:key (services %embix-services)
                         (packages %base-packages))
  (operating-system
    (host-name name)
    (timezone %embix-tz)
    (locale "en_US.utf8")
    (sudoers-file %embix-sudoers)
    (keyboard-layout %embix-kbd)
    (bootloader %embix-bootloader)
    (file-systems %embix-fs)
    (users %embix-users)
    (services (modify-services services
                (openssh-service-type config => 
                  (openssh-configuration
                    (inherit config)
                    (authorized-keys
                      `(("vmer", ssh-pub-key)))))))
    (packages packages)))

;; vim: sw=2:et:ai
