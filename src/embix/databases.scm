;;;
;;; embix - collection of packages and functions for Guix
;;;
;;; Copyright (C) 2023 by Artur Wroblewski <wrobell@riseup.net>
;;; Some code copied from gnu/packages/python.scm of Guix project.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

(define-module (embix databases)
  #:use-module ((embix licenses) #:prefix licenses:)
  #:use-module (gnu packages)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages tls)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:export (timescaledb
            postgresql-15.3))

(define postgresql-15.3
  (package
    (inherit postgresql-15)
    (version "15.3")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://ftp.postgresql.org/pub/source/v"
                          version "/postgresql-" version ".tar.bz2"))
      (sha256 (base32 "0cnrk5jrwfqkcx8mlg761s60ninqrsxpzasf7xfbzzq03y4x9izz"))
      (patches (search-patches "postgresql-disable-resolve_symlinks.patch"))))))

;; package is based on timescaledb package from Guix project, but uses
;; Timescale License
(define-public timescaledb-2.11
  (package
    (name "timescaledb")
    (version "2.11.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/timescale/timescaledb")
                     (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1q5qwbldnn5f0sdji5m0mim5qdh6y714cqpvg90gk77pgivqw008"))
              (modules '((guix build utils)))))
    (build-system cmake-build-system)
    (arguments
     (list #:imported-modules `((guix build union)
                                ,@%cmake-build-system-modules)
           #:modules `(,@%cmake-build-system-modules
                       (guix build union)
                       (ice-9 match))
           #:configure-flags #~(list "-DAPACHE_ONLY=OFF"
                                     "-DREGRESS_CHECKS=OFF"
                                     "-DSEND_TELEMETRY_DEFAULT=OFF")
           #:phases
           #~(modify-phases (@ (guix build cmake-build-system) %standard-phases)
               (add-after 'unpack 'patch-install-location
                 (lambda _
                   ;; Install extension to the output instead of the
                   ;; PostgreSQL store directory.
                   (substitute* '("CMakeLists.txt"
                                  "cmake/GenerateScripts.cmake"
                                  "sql/CMakeLists.txt")
                     (("\\$\\{PG_SHAREDIR\\}/extension")
                      (string-append #$output "/share/extension")))
                   ;; Likewise for the library.
                   (substitute* '("src/CMakeLists.txt"
                                  "src/loader/CMakeLists.txt"
                                  "tsl/src/CMakeLists.txt")
                     (("\\$\\{PG_PKGLIBDIR\\}")
                      (string-append #$output "/lib")))))
               (add-after 'unpack 'remove-kernel-version
                 ;; Do not embed the running kernel version for reproducible
                 ;; builds
                 (lambda _
                   (substitute* "src/config.h.in"
                     (("BUILD_OS_VERSION ..CMAKE_SYSTEM_VERSION.")
                      "BUILD_OS_VERSION \""))))
               ;; Run the tests after install to make it easier to create the
               ;; required PostgreSQL+TimescaleDB filesystem union.
               (delete 'check))))
    (inputs (list openssl postgresql-15))
    (home-page "https://www.timescale.com/")
    (synopsis "Time-series extension for PostgreSQL")
    (description
     "TimescaleDB is a database designed to make SQL scalable for
time-series data.  It is engineered up from PostgreSQL and packaged as a
PostgreSQL extension, providing automatic partitioning across time and space
(partitioning key), as well as full SQL support.")
    (license licenses:tsl)))

;; vim: sw=2:et:ai
