;;;
;;; embix - collection of packages and functions for Guix
;;;
;;; Copyright (C) 2023 by Artur Wroblewski <wrobell@riseup.net>
;;; Some code copied from gnu/packages/python.scm of Guix project.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

(define-module (embix python)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages python)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)

  #:export (wrap-python3))

(define-public python-3.11
  (package
    (inherit python-3.10)
    (name "python")
    (version "3.11.3")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://www.python.org/ftp/python/"
                          version "/Python-" version ".tar.xz"))
      (patches (search-patches
               "python-3-deterministic-build-info.patch"
               "python-3-hurd-configure.patch"
               "python-3-search-paths.patch"))
      (sha256 (base32 "0pjv10fgznq0p7ifmnqi3xsnij82jsf1hmjrqwkwyzhsjsfbjpca"))
      (modules '((guix build utils)))))
    (arguments
      ;; (substitute-keyword-arguments (package-arguments python-3.10)
      ;;   ((#:tests? _ #f) #f)))))
      ;;
      ;; above fails with
      ;;     starting phase `remove-tests'
      ;;     error: in phase 'remove-tests': uncaught exception:
      ;;     match-error "match" "no matching pattern" ("python3.10" "python3.11")
      ;;
      ;; anyway, there are so many tests excluded by Guix's python-3.10
      ;; package, that there is no point to run them really
      `(#:phases
        (modify-phases %standard-phases
          (delete 'check))))))

;; copied from Guix project
(define* (wrap-python3 python
                       #:optional
                       (name (string-append (package-name python) "-wrapper")))
  (package/inherit python
    (name name)
    (source #f)
    (build-system trivial-build-system)
    (outputs '("out"))
    (inputs `(("bash" ,bash)))
    (propagated-inputs `(("python" ,python)))
    (arguments
     (list #:modules '((guix build utils))
           #:builder
           #~(begin
               (use-modules (guix build utils))
               (let ((bin (string-append #$output "/bin"))
                     (python (string-append
                              ;; XXX: '%build-inputs' contains the native
                              ;; Python when cross-compiling.
                              #$(if (%current-target-system)
                                    (this-package-input "python")
                                    #~(assoc-ref %build-inputs "python"))
                              "/bin/")))
                 (mkdir-p bin)
                 (for-each
                  (lambda (old new)
                    (symlink (string-append python old)
                             (string-append bin "/" new)))
                  `("python3" ,"pydoc3" ,"pip3")
                  `("python"  ,"pydoc"  ,"pip"))
                 ;; python-config outputs search paths based upon its location,
                 ;; use a bash wrapper to avoid changing its outputs.
                 (let ((bash (string-append (assoc-ref %build-inputs "bash")
                                            "/bin/bash"))
                       (old  (string-append python "python3-config"))
                       (new  (string-append bin "/python-config")))
                   (with-output-to-file new
                     (lambda ()
                       (format #t "#!~a~%" bash)
                       (format #t "exec \"~a\" \"$@\"~%" old)
                       (chmod new #o755))))))))
    (synopsis "Wrapper for the Python 3 commands")
    (description
     "This package provides wrappers for the commands of Python@tie{}3.x such
that they can also be invoked under their usual names---e.g., @command{python}
instead of @command{python3} or @command{pip} instead of @command{pip3}.

To function properly, this package should not be installed together with the
@code{python} package: this package uses the @code{python} package as a
propagated input, so installing this package already makes both the versioned
and the unversioned commands available.")))

(define-public python-wrapper-3x
  (wrap-python3 python-3.11))

;; vim: sw=2:et:ai
